/**
 * [decodeCombo description]
 *
 * @return  {[type]}  [return description]
 */
function decodeCombo(comboUrl){
    const [,comboArr] = comboUrl.split(/\?\?/);
    if(comboArr){
        return comboArr.split(',');
    }
    return null;
}
console.log(decodeCombo('https://g.alicdn.com/tb-mod/??tb-pad/6.2.0/index.css,tb-sitenav/6.2.0/index.ccs,tb-sysinfo/6.2.0/index.css,tb-sysbamner/6.2.0/index.css,tb-bamner/6.2.0/index.css,tb-birthday/2.1/index.css,tb-search/6.2.9/index.css,tb-logo/6.2.css'));
/**
 * [EventEitter 发布订阅]
 */
class EventEitter{
    constructor(){
        this.events = {}
    }
    on(name,callback){ //
        if(Object.prototype.toString.call(this.events[name]) === '[object Array]'){
            this.events[name].push(callback);
        }else{
            this.events[name] = [callback];
        }
    }
    trigger(name,...arg){
        this.events[name].forEach(callback=>{
            callback.call(this,...arg);
        })
    }
    off(name,callback){
        const index = this.events[name].findIndex(item=>item===callback);
        if(index < 0){
            return;
        }
        this.events[name].splice(index,1);
    }
}


const event = new EventEitter();

const handleUpdate = function(time,t1){  
    console.log('handleUpdate',time,t1,this.events);
}

event.on('updata',handleUpdate);
event.off('updata',handleUpdate);
event.on('updata',(time,t1)=>{  
    console.log('second',time,t1)
})

event.trigger('updata','2020-6-8','20:50');


/**
 * [BigInt 求两个大整数之和]
 */
class BigInt{
    constructor(str){
        if(!/^\d+$/.test(str)){
           throw '不是一个合法得数字' ;
        }
        this.content = str;
    }
    plus(bigint){
        return this.add(this.content,bigint.content);
    }
    add(a, b) {
        if (a.length < b.length) {
            a = '0' + a;
        }
        if (b.length < a.length) {
            b = '0' + b;
        }

        // 标志位 满十进一
        let addOne = 0;
        let res = [];
        for (let i = a.length; i >= 0; i--) {
            //像小学加法那样，从最后一位开始相加
            let c1 = a.charAt(i) - 0;
            let c2 = b.charAt(i) - 0;
            let sum = c1 + c2 + addOne;
            //满十进一
            if (sum > 9) {
                addOne = 1;
                res.unshift(sum - 10);
            } else {
                addOne = 0;
                res.unshift(sum);
            }
        }
        // 如果是1，就进一
        if (addOne) {
            res.unshift(addOne);
        }
        // 假如这种情况，'01'+'02' = '03'
        if (res[0] === 0) {
            res.splice(0, 1);
        }
        //数组变字符串
        return res.join('');
    }
}
const bigint1 = new BigInt('1234343431212121214454545454221212');
const bigint2 = new BigInt('1234343431212121214454545454221212');
console.log(bigint1.plus(bigint2));

/**
 * [routeParse 路由名解析]
 *
 * @return  {[type]}  [return 对象]
 */
function routeParse(parseUrl,url){
    let parseArr = parseUrl.split('/');
    let urlArr = url.split('/');
    if(parseArr.length !== urlArr.length){
        return null;
    }
    let res = null;
    for(let i = 1;i< parseArr.length;i++){
        if((!/^:/.test(parseArr[i])) && (urlArr[i]===parseArr[i])){
            res = {}
        }else if(/^:/.test(parseArr[i])){
            res[parseArr[i].substr(1)] = urlArr[i]
        }else{
            break;
        }
    }
    return res;
}

console.log(routeParse('/user/:id','/user/10'));
console.log(routeParse('/user','/user'));
console.log(routeParse('/user/:id/:a','/user/10/1'));
console.log(routeParse('/user/:id/:a','/list/10/1'));
console.log(routeParse('/user/:id','/user'));


//d.hh:mm:ss

function secondsTimes(times){
    const addZero = (a)=>a < 10 ? '0'+a : a;
    if(times && times !== Infinity){
        let d = parseInt(times / 86400);
        let hh = parseInt(times % 86400 / 60 / 60)
        let mm = parseInt(times % 86400 % 3600 / 60)
        let ss = times % 86400 % 3600 % 60;
        return `${d}.${addZero(hh)}:${addZero(mm)}:${addZero(ss)}`;
    }
    return ''
}
console.log(secondsTimes(100000))
console.log(secondsTimes(862100))

console.log(secondsTimes(72))
console.log(secondsTimes(0))
console.log(secondsTimes())
console.log(secondsTimes(Infinity))


/**
 * 你是专业小偷，今天你打算偷遍一整条街的房子，每间房了都有不同数量的钱可以偷，但是房子之间有报警系统，如果你连偷两间相邻的房子，就会引来警察。
 * 这里有一个List,每个元素都代表这间房子内可以偷到的钱，你要如何安排计划才能偷到最多的钱而且不会惊动警察。
 * [2,4,5,1,3]， 最多可以偷到2+5+3 = 10， 因为4+5+3 = 12虽然可以拿到比较多钱，但是会被警察抓
 * 定义lines函数得到 最多金额
 */

function lines(arr=[]){
    if(arr.length == 0) return 0;
    let len = arr.length;
    if(len == 1) return arr[0];
    let res = [];
    res[0] = arr[0];
    res[1] = Math.max(arr[0], arr[1]);
    for(let i= 2; i< len; i++){
        res[i] = Math.max(res[i-1], arr[i] + res[i-2]);
    }
    return res[len-1];
}   

console.log(lines([2,4,5,1,3]));
