```json
// 配置文件craco.config.js
// antD按需加载 重载webpack  npm run eject
"@craco/craco": "^6.4.2",   
// 规定Git commit提交规范 git commit -m'feat 修改' 'fix ' 'bug '
"@commitlint/cli": "^13.1.0",
"@commitlint/config-conventional": "^13.1.0",
// git cz commitlint.config.js
"commitizen": "^4.2.4", 
// 设置环境变量 cross-env = "development"
"cross-env": "^7.0.3",
// 代码规范 prettier代码基本结构
// react-Hooks的写法 .eslintrc
// .prettierrc
"eslint": "7.32.0",
"eslint-config-prettier": "^8.3.0",
"eslint-config-standard": "^16.0.3",
"eslint-plugin-react-hooks": "^4.3.0",
"eslint-plugin-flowtype": "^5.9.2",
"eslint-plugin-import": "^2.24.2",
"eslint-plugin-node": "^11.1.0",
"eslint-plugin-prettier": "^4.0.0",
"eslint-plugin-promise": "^5.1.0",
"eslint-plugin-react": "^7.25.1",
"eslint-plugin-standard": "^5.0.0",
"husky": "^7.0.2",
"lint-staged": "^10.0.7",
"prettier": "^2.2.0",
// babel 转译插件
"babel-plugin-import": "^1.13.3",
"less-loader": "4.1.0",
"ts-import-plugin": "^2.0.0",
// 规定样式规范 .stylelintrc
"stylelint": "^13.13.1",
"stylelint-config-prettier": "^8.0.2",
"stylelint-config-standard": "^22.0.0",



"husky": { // 规定git的提交规范和代码规范
  "hooks": {
    "pre-commit": "lint-staged",
    "commit-msg": "commitlint -E HUSKY_GIT_PARAMS"
  }
},
"lint-staged": {
  "*.{js,jsx}": [
    "eslint --fix",
    "prettier --write",
    "git add"
  ],
  "*.ts?(x)": [
    "eslint --fix",
    "prettier --parser=typescript --write",
    "git add"
  ],
  "*.less": [
    "stylelint --syntax less",
    "prettier --write",
    "git add"
  ]
},
// git cz
"config": {
  "commitizen": {
    "path": "cz-conventional-changelog"
  }
},
// webpack的优化项  import sum from 'xx' 
"browserslist": {
  "production": [
    ">0.2%",
    "not dead",
    "not op_mini all"
  ],
  "development": [
    "last 1 chrome version",
    "last 1 firefox version",
    "last 1 safari version"
  ]
},
```

