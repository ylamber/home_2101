const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin');//压缩css文件
const ESLintPlugin = require('eslint-webpack-plugin');
const Webpack = require('webpack')

const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
  mode: 'development',
  entry: path.resolve(__dirname,'src'),
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: "bundle.js",
    // 配置服务器域名，将打包好的css，html路径前加上配置的名字
    // publicPath: 'http://www.yl.cn'
  },
  devServer: {
    port: 3000,
    contentBase: path.join(__dirname, 'dist'),
    progress: true,
    compress: true,
    // 解决跨域
    // proxy: {
    //   '/api': {
    //     target: 'http://localhost:8080',
    //     pathRewrite: {
    //       '^/api': ''
    //     }
    //   }
    // }
    // 默认请求
    // before: () => {}
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src/index.html'),
      filename: 'index.html',
      // minify: {
      //   // 删除html中的双引号
      //   removeAttributeQuotes: true,
      //   // 将html打包的文件折叠成一行
      //   // collapseWhitespace: true
      // }
      minify: false
    }),
    new MiniCssExtractPlugin({
      filename: 'css/main.css'
    }),
    // new OptimizeCssAssetsWebpackPlugin(),
    // 配置eslint强制校验 在eslint官网上选择就可以
    // new ESLintPlugin()
    // 直接使用$，拿到jquery
    new Webpack.ProvidePlugin({
      $: 'jQuery'
    }),
    // 负责Copy其他目录文件
    // new CopyWebpackPlugin({
    //   patterns: [
    //     { from: "doc", to: "dest" },
    //   ],
    // }),
    // 负责在打包的文件中加入注释
    // new Webpack.BannerPlugin('make 2021 by yl')
  ],
  // 使用CDN方式引入，不需要强制打包
  externals: {
    jquery: 'jQuery'
  },
  module: {
    rules: [
      // { 现在使用插件的形式配置
      //   test: /\.js$/,
      //   use: {
      //     loader: 'eslint-loader',
      //     options: {
      //       enfore: 'pre' // 将代码先执行
      //     }
      //   }
      // },
      {
        test: require.resolve("jquery"),
        loader: "expose-loader",
        options: {
          exposes: ["$", "jQuery"],
        },
      },
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              '@babel/preset-env'
            ],
            plugins: [
              ["@babel/plugin-proposal-decorators", { "legacy": true }],
              ["@babel/plugin-proposal-class-properties"],
              ["@babel/plugin-transform-runtime"],
              ["@babel/plugin-transform-modules-commonjs"]
            ]
          }
        },
        include: path.resolve(__dirname,'src'),
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          "postcss-loader",
        ]
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          "sass-loader"
        ]
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            // 不会发起http请求,但是file-loader会发起http请求
            loader: 'url-loader',
            options: {
              limit: 200*1204,
              // 超过限制才会输出到指定目录
              outputPath: '/img/',
              // 配置服务器域名，将打包好的css，html路径前加上配置的名字
              // publicPath: 'http://www.yl.cn'
            }
          },
        ],
      },
      {
        test: /\.(htm|html)$/,
        loader: 'html-withimg-loader'
　　　　},
    ]
  },
  // 增加映射文件，可以帮助我们调试代码, 出错会标识当前的错误在哪一行
  // devtool: 'source-map',
  // "eval-source-map" 不会产生单独文件， 但是可以显示行和列
  // "cheap-module-source-map" 不会产生列，但是会产生单独的映射文件
  // "cheap-module-eval-source-map" 不会产生文件但是会继承在文件中

  // 实时打包，并且可以渲染页面
  // watch: true,
  // watchOptions: { // 监控的选项
  //   poll: 1000, // 每秒监控更新
  //   aggregateTimeout: 500, // 防抖
  //   ignored: /node_modules/
  // }

  resolve: { // 解析 第三方包
    modules: [path.resolve('node_modules')],
    // 配置查找的主入口的字段
    mainFields: ['style','main'],
    // alias: {
    //   bootstrap: 'bootstrap/dist/css/bootstrap.css'
    // }
    // 扩展名
    extensions: ['.css']
  }
}