const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const Webpack = require('webpack')
// vue-template-compiler 用于编译模板
const vueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = {
  // "production" 生产 | "development" 开发 
  mode: "development",
  // string | object | array
  /* 
    babel-loader只会将 ES6/7/8语法转换为ES5语法，
    但是对新api并不会转换 例如(promise、Generator、Set、Maps、Proxy、includes等)
    此时我们需要借助babel-polyfill来帮助我们转换
  */
  entry: ["@babel/polyfill", "./src/index.js"],
  output: {
    // 输出的资源路径，必须是绝对路径（使用 Node.js 的 path 模块）
    path: path.resolve(__dirname, "dist"),
    // 多出口文件时，使用[name].[chunkhash].js
    filename: "bundle.js",
  },
  module: {
    // 关于模块配置
    rules: [
      // 模块规则（配置 loader、解析器等选项）
      {
        test: /\.css$/i,
        // css-loader 把css模块内容加入到js模块中
        // style-loader 把css的内容加载到html的style标签中
        use: [
          // 'style-loader',
          MiniCssExtractPlugin.loader,
          'css-loader',
          // 处理css前缀 结合autoprefixer使用
          'postcss-loader'
        ],
        include: [path.resolve(__dirname, 'src')]
      },
      {
        test: /\.jsx?$/,
        use: [
          {
            loader: 'babel-loader',
          }
        ],
        exclude: /node_modules/ //排除 node_modules 目录
      },
      {
        test: /\.s[ac]ss$/i,
        // css-loader 把css模块内容加入到js模块中
        // style-loader 把css的内容加载到html的style标签中
        use: [
          // 'style-loader',
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            // css 模块化
            // options: {
            //   modules: true
            // }
          },
          'postcss-loader',
          'sass-loader'
        ],
        include: [path.resolve(__dirname, 'src')]
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10240,
              fallback: {
                loader: 'file-loader',
                options: {
                  name: '[name]_[hash:6].[ext]',
                  outputPath: "images/"
                }
              }
            }
          },
        ],
      },
      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/, //媒体文件
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10240,
              fallback: {
                loader: 'file-loader',
                options: {
                  name: '[name].[hash:8].[ext]',
                  outputPath: "media/"
                }
              }
            }
          }
        ]
      },
      {
        test: /\.(woff2|eot|ttf|otf)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[hash:8].[ext]',
              outputPath: "fonts/"
            }
          }
        ]
      },
      // {
      //   test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/i, // 字体
      //   use: [
      //     {
      //       loader: 'url-loader',
      //       options: {
      //         limit: 10240,
      //         fallback: {
      //           loader: 'file-loader',
      //           options: {
      //             name: '[name].[hash:8].[ext]',
      //             outputPath: "fonts/"
      //           }
      //         }
      //       }
      //     }
      //   ]
      // }
      {
        test:/\.js$/,
        use:{
          loader:'babel-loader',
          options:{
            presets:['@babel/preset-env']
          }
        },
        exclude:/node_modules/
      },
      {
        test:/\.vue$/,
        use:['vue-loader']
      },
    ]
  },
  // 通过在浏览器调试工具(browser devtools)中添加元信息(meta info)增强调试
  // 牺牲了构建速度的 `source-map' 是最详细的。
  // devtool: "source-map",
  // 插件安装
  plugins: [
    new HtmlWebpackPlugin({
      title: 'webpack@4配置',
      template: './public/index.html',
      filename: 'index.html', //打包后的文件名
      minify: {
        removeAttributeQuotes: false, //是否删除属性的双引号
        collapseWhitespace: false, //是否折叠空白
      },
      // hash: true //是否加上hash，默认是 false
    }),
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: "[name].[hash].css",
      chunkFilename: "[id].css",
    }),
    new Webpack.HotModuleReplacementPlugin(),
    new vueLoaderPlugin()
  ],
  // 选择一种 source map 格式来增强调试过程。不同的值会明显影响到构建(build)
  // 和重新构建(rebuild)的速度。
  // 配置推荐 开发环境 - "eval-cheap-module-source-map"
  // 线上配置不推荐开启
  devtool: '',
  devServer:{
    port:3000,
    hot:true,
    contentBase: path.resolve(__dirname, 'dist'),
    quiet: true
  },
  resolve:{
    alias:{
      'vue$':'vue/dist/vue.runtime.esm.js',
      ' @':path.resolve(__dirname,'../src')
    },
    extensions:['*','.js','.json','.vue']
  },
}