const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin');//压缩css文件
const ESLintPlugin = require('eslint-webpack-plugin');
const Webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
  mode: 'development',
  entry: path.resolve(__dirname,'src'),
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: "bundle.js",
    // 配置服务器域名，将打包好的css，html路径前加上配置的名字
    // publicPath: 'http://www.yl.cn'
  },
  devServer: {
    port: 3000,
    contentBase: path.join(__dirname, 'dist'),
    progress: true,
    compress: true,
    // 解决跨域
    // proxy: {
    //   '/api': {
    //     target: 'http://localhost:8080',
    //     pathRewrite: {
    //       '^/api': ''
    //     }
    //   }
    // }
    // 默认请求
    // before: () => {}
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src/index.html'),
      filename: 'index.html',
      minify: false
    }),
    new MiniCssExtractPlugin({
      filename: 'css/main.css'
    }),
    new Webpack.ProvidePlugin({
      $: 'jQuery'
    }),
    new Webpack.DefinePlugin({
      // DEV: "'dev'"
      DEV: JSON.stringify('dev')
    })
  ],
  // 使用CDN方式引入，不需要强制打包
  externals: {
    jquery: 'jQuery'
  },
  module: {
    rules: [
      {
        test: require.resolve("jquery"),
        loader: "expose-loader",
        options: {
          exposes: ["$", "jQuery"],
        },
      },
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              '@babel/preset-env'
            ],
            plugins: [
              ["@babel/plugin-proposal-decorators", { "legacy": true }],
              ["@babel/plugin-proposal-class-properties"],
              ["@babel/plugin-transform-runtime"],
              ["@babel/plugin-transform-modules-commonjs"]
            ]
          }
        },
        include: path.resolve(__dirname,'src'),
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          "postcss-loader",
        ]
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          "sass-loader"
        ]
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            // 不会发起http请求,但是file-loader会发起http请求
            loader: 'url-loader',
            options: {
              limit: 200*1204,
              // 超过限制才会输出到指定目录
              outputPath: '/img/',
              // 配置服务器域名，将打包好的css，html路径前加上配置的名字
              // publicPath: 'http://www.yl.cn'
            }
          },
        ],
      },
      {
        test: /\.(htm|html)$/,
        loader: 'html-withimg-loader'
　　　　},
    ]
  },
  
}