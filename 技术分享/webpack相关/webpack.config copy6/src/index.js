
// webpack 引入第三方变量
// import $ from 'jquery'
// console.log($);


// const a = require('./a')
// require('./index.css')
// require('./index.scss')

// console.log("webpack-cli | ",a.ssr);
// let fn = () => {
//   console.log("执行fn");
// }
// fn()


// function log(options) {
//   console.log(options,"options");
// }

// @log
// class A{
//   a = 1;
// }

// console.log(new A().a);

// webpack打包图片
// 1、在js中创建图片引入
// import logo from './小丑.jpg'
// import './a.css'
// let image = new Image()
// image.src = logo
// image.style = "width:200px;height:200px"
// document.body.appendChild(image)
// 2、在css中引入
// 3、在img标签中引入


// webpack跨域问题
// let xhr = new XMLHttpRequest();
// xhr.open('GET','/api/user',true)
// xhr.onload = function () {
//   console.log(xhr.response);
// }
// xhr.send()

// import 'bootstrap'

// import './index'


// let url = ''
// // DEV 来自webpack配置的全局的环境变量
// if(DEV === 'dev'){
//   url = "http://www.google.com"
// }else{
//   url = "http://www.baidu.com"
// }
// console.log(url,"-----");


// noParse 不去解析不需要加载的包
// import j from 'jquery'

// IgnorePlugin
// import moment from 'moment'
// // 当使用IgnorePlugin忽略moment语言包时，可以自己手动引入
// // 按需引入
// import 'moment/locale/zh-cn'
// moment.locale('zh-cn')
// let r = moment().endOf('day').fromNow();
// console.log(r);

// 动态连接库
import React from 'react'
import {render} from 'react-dom'
render(<h1>jsx</h1>, window.root)