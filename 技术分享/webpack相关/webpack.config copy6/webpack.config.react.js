const path = require('path');
const Webpack = require('webpack');

module.exports = {
  mode: 'development',
  entry: {
    react: ['react', 'react-dom']
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: "_dll_[name].js",
    // 给输出到main.js的文件添加全局的变量
    library: '_dll_[name]',
    // libraryTarget: 'var' 默认为var commonjs this umd ....
  },
  plugins: [
    new Webpack.DllPlugin({ // name === library: '_dll_[name]'
      name: '_dll_[name]',
      path: path.resolve(__dirname, 'dist', 'manifest.json')
    })
  ]
}