const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const Webpack = require('webpack');

// 模块 Happypack 可以实现多线程打包
// const Happypack = require('happypack')

// tree-shaking 作用： 当使用import语法导入其他模块的时候
// 并且mode为生产模式的时候会把没用的代码自动删除掉
// 但是开发模式的时候不会自动删除多余代码
/*
 - test.js export default {sum,minus}
 - index.js import calc from './test.js'
*/
// scope hosting 作用域提升
/* 
  let a = 1;
  let b = 1;
  let c = a + b
  console.log(c)
*/

module.exports = {
  mode: 'development',
  entry: path.resolve(__dirname,'src'),
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: "bundle.js",
    // 配置服务器域名，将打包好的css，html路径前加上配置的名字
    // publicPath: 'http://www.yl.cn'
  },
  devServer: {
    port: 3000,
    contentBase: path.join(__dirname, 'dist'),
    progress: true,
    compress: true,
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src/index.html'),
      filename: 'index.html',
      minify: false
    }),
    new MiniCssExtractPlugin({
      filename: 'css/main.css'
    }),
    // 配置第三方库的安装
    new Webpack.ProvidePlugin({
      $: 'jQuery'
    }),
    // 设置环境变量
    new Webpack.DefinePlugin({
      // DEV: "'dev'"
      DEV: JSON.stringify('dev')
    }),
    // 忽略第三方库的加载文件
    new Webpack.IgnorePlugin({
      resourceRegExp: /^\.\/locale$/,
      contextRegExp: /moment$/,
    }),
    // 使用动态连接库 需要现在index.html中引用生成的js文件
    new Webpack.DllReferencePlugin({
      manifest: path.resolve(__dirname, 'dist', 'manifest.json')
    }),
    // 使用Happypack 项目较大时推荐使用
    // new Happypack({
    //   id: 'js',
    //   use: [
    //     {
    //       loader: 'babel-loader',
    //       options: {
    //         presets: [
    //           '@babel/preset-env',
    //           '@babel/preset-react'
    //         ],
    //         plugins: [
    //           ["@babel/plugin-proposal-decorators", { "legacy": true }],
    //           ["@babel/plugin-proposal-class-properties"],
    //           ["@babel/plugin-transform-runtime"],
    //           ["@babel/plugin-transform-modules-commonjs"]
    //         ]
    //       }
    //     }
    //   ]
    // })
  ],
  module: {
    noParse: /jquery/, // 不去解析地方库的依赖关系
    rules: [
      {
        test: require.resolve("jquery"),
        loader: "expose-loader",
        options: {
          exposes: ["$", "jQuery"],
        },
      },
      // {
      //   test: /\.js$/,
      //   use: 'Happypack/loader?id=js',
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              '@babel/preset-env',
              '@babel/preset-react'
            ],
            plugins: [
              ["@babel/plugin-proposal-decorators", { "legacy": true }],
              ["@babel/plugin-proposal-class-properties"],
              ["@babel/plugin-transform-runtime"],
              ["@babel/plugin-transform-modules-commonjs"]
            ]
          }
        },
        // 只去查找哪些库
        include: path.resolve(__dirname,'src'),
        // 不去查找相关目录
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          "postcss-loader",
        ]
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          "sass-loader"
        ]
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            // 不会发起http请求,但是file-loader会发起http请求
            loader: 'url-loader',
            options: {
              limit: 200*1204,
              // 超过限制才会输出到指定目录
              outputPath: '/img/',
              // 配置服务器域名，将打包好的css，html路径前加上配置的名字
              // publicPath: 'http://www.yl.cn'
            }
          },
        ],
      },
      {
        test: /\.(htm|html)$/,
        loader: 'html-withimg-loader'
　　　　},
    ]
  }
}