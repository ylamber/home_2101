const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin');//压缩css文件
const TerserWebpackPlugin = require('terser-webpack-plugin')

process.env.NODE_ENV="development"

module.exports = {
  mode: 'production',
  entry: path.resolve(__dirname,'src'),
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: "bundle.js"
  },
  devServer: {
    port: 3000,
    contentBase: path.join(__dirname, 'dist'),
    progress: true,
    compress: true
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src/index.html'),
      filename: 'index.html',
      // minify: {
      //   removeAttributeQuotes: true,
      //   collapseWhitespace: true
      // }
    }),
    new MiniCssExtractPlugin({
      filename: 'main.css'
    }),
    new OptimizeCssAssetsWebpackPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          // {
          //   loader: 'style-loader',
          //   // options: {
          //   //   insert: 'top'
          //   // }
          // },
          MiniCssExtractPlugin.loader,
          'css-loader',
          "postcss-loader",
        ]
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          // "postcss-loader",
          // {
          //   loader: "postcss-loader",
          //   options: {
          //     postcssOptions: {
          //       plugins: [
          //         [
          //           require('postcss-preset-env')()
          //         ],
          //       ],
          //     },
          //   },
          // },
          "sass-loader"
        ]
      }
    ]
  },
  optimization: {
    minimize: true,
    minimizer: [
      new TerserWebpackPlugin(),
      new OptimizeCssAssetsWebpackPlugin()
    ]
  },
}