const { merge } = require('webpack-merge');
const common = require('./webpack.base');
// 运行命令 npm run build -- --config webpack.dev.js
module.exports = merge(common, {
  mode: 'development'
})