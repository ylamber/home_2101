#### HTML

- h5新特性

> 音频，视频，文件上传下载

- meta属性

> 性能优化有关

#### CSS

- 伪类，伪元素
- CSS动画，transform; animation; transition; GPU
- requestAnimationForm; 
- BFC - Block Formmating Context
- 响应式布局 - 100vh 100vw  百分比 Flex Grid

#### JavaScript

- 手写源码系列 

> Promise .all .race   并发
>
> Call & Apply & Bind
>
> 深拷贝
>
> 防抖&节流

- 数据结构和算法

> 数组扁平化

- 原型&原型链&作用域&this指向
- 事件循环(必知必会)

#### 浏览器原理

> http1.0 & http1.1 区别
>
> 项目中如何实现跨域
>
> 缓存 Vuex  Redux 浏览器缓存 (强缓存和协商缓存)   
>
> 浏览器渲染 重绘 &  重排； 防抖 & 节流 ； Url到页面渲染

#### 性能优化

> DNS解析 & px转rem 插件 =》配置在webpack中
>
> preload 预加载  prefetch 预请求

> 图片懒加载；服务器端渲染 SSR  (Nuxt，Next，微前端)

#### Table表格 ElementUI & AntD   后台管理系统  

#### Form表单 ElementUI & Vant    后台管理系统 & 移动端

#### Echarts 和 地图组件 的二次封装

#### 富文本编辑器（wangEditor）二次封装 

#### 基本排版 - 分页，按钮，上拉加载&下拉刷新，埋点(图片)

#### 鉴权 (双token，按钮鉴权，路由鉴权 )

#### 大文件上传下载 （音频，视频）

#### 图片裁剪上传 & 数据导入导出

#### 聊天室 (一对一，一对多，多对多)

#### 弹框（各种模态框）